package main

const BIFS_GO_TEMPLATE = `// {{ .OutputFile }}
// 由 mkbifs 自动生成。请勿修改。
//
package {{ .Package }}

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
)

const GZIPD = {{ .Gzipd }}

// 内置的文件存放在一个 BIFile 结构中。 
// 
type BIFile struct {
	data string //数据
	name string //文件名
	mime string //文件类型
}

type BIFiles map[string]BIFile

var _FS = BIFiles{}

func init() {
	// 初始化_FS
	{{ range .Files }}
	_FS[{{ .Name }}] = BIFile{
		name: {{ .Name }},
		data: {{ .Data }},
		mime: {{ .MIME }},
	}
	{{ end }}
}

// 读出内置文件的列表
func ReadDir() BIFiles {
	return _FS
}

// 读出一个文件中的数据
func ReadFile(name string) string {
	if _, ok := _FS[name]; ok {
		file := _FS[name]
		return file.ReadAll()
	} else {
		return ""
	}
}

// Open() 不需要 Close()
func Open(name string) BIFile {
	return _FS[name]
}

func (b *BIFile) ReadAll() string {
	data, _ := b.decode()
	return string(data)
}

func (b *BIFile) Name() string {
	return b.name
}

func (b *BIFile) MIME() string {
	return b.mime
}

// 反编码数据
//
func (b *BIFile) decode() ([]byte, error) {
	// BIFile.data -> 压缩后数据 -> gunzip -> 原始数据
	if GZIPD {
		var buf bytes.Buffer
		// gunzip
		gz, err := gzip.NewReader(bytes.NewBuffer([]byte(b.data)))
		if err != nil {
			return nil, fmt.Errorf("BIFS: GUNZIP ERROR %q: %v", b.name, err)
		}
		// write to buf
		_, err = io.Copy(&buf, gz)
		gz.Close()

		if err != nil {
			return nil, fmt.Errorf("BIFS: WRITE OUT ERROR %q: %v", b.name, err)
		}

		return buf.Bytes(), nil
	}
	return []byte(b.data), nil
}`
